{{-- <div class="panel panel-primary">

        <div class="panel-heading">Chart</div>
        <div class="panel-body">
                {!! $chart->html() !!}
        </div>
    </div>
    {!! Charts::scripts()!!}
    {!! $chart->script() !!}  <div class="panel panel-primary">

        <div class="panel-heading">Chart</div>
        <div class="panel-body">
                {!! $chart->html() !!}
        </div>
    </div>
    {!! Charts::scripts()!!}
    {!! $chart->script() !!} --}}

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">

      var visitor = <?php echo $visitor; ?>;

      console.log(visitor);

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(visitor);

        var options = {

          title: 'Soil Fertility [NPK] Chart',

          curveType: 'function',

          legend: { position: 'bottom' }

        };

        var chart = new google.visualization.LineChart(document.getElementById('linechart'));

        chart.draw(data, options);

      }

    </script>

    <div id="linechart" style="width: 900px; height: 500px"></div>