<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="/"><i class="fa fa-edit"></i><span>Home</span></a>
</li>
@if(Auth::user()->role_id==1)
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('systems*') ? 'active' : '' }}">
    <a href="{!! route('systems.index') !!}"><i class="fa fa-edit"></i><span>Systems</span></a>
</li>

<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>

<li class="{{ Request::is('statuses*') ? 'active' : '' }}">
    <a href="{!! route('statuses.index') !!}"><i class="fa fa-edit"></i><span>Statuses</span></a>
</li>
@endif
<li class="{{ Request::is('farms*') ? 'active' : '' }}">
        <a href="{!! route('farms.index') !!}"><i class="fa fa-edit"></i><span>Farms</span></a>
    </li>
