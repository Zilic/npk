@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            System
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($system, ['route' => ['systems.update', $system->id], 'method' => 'patch']) !!}

                        @include('systems.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection