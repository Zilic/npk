<div class="table-responsive">
    <table class="table" id="systems-table">
        <thead>
            <tr>
                <th>Name</th>
      
        <th>Model</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($systems as $system)
            <tr>
                <td>{!! $system->name !!}</td>
          
            <td>{!! $system->model !!}</td>
            <td>{!! $system->status['name'] !!}</td>
                <td>
                    {!! Form::open(['route' => ['systems.destroy', $system->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('systems.show', [$system->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('systems.edit', [$system->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
