<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $system->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $system->name !!}</p>
</div>

<!-- Lname Field -->
<div class="form-group">
    {!! Form::label('lname', 'Lname:') !!}
    <p>{!! $system->lname !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $system->model !!}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status:') !!}
    <p>{!! $system->status['name'] !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Time:') !!}
    <p>{!! $system->created_at !!}</p>
</div>

