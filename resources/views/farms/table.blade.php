<div class="table-responsive">
    <table class="table" id="farms-table">
        <thead>
            <tr>
                <th>Nitrogen</th>
        <th>Phosphorus</th>
        <th>Potasium</th>
        <th>Time</th>
    <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($farms as $farm)
            <tr>
                <td>{!! $farm->nitrogen !!}</td>
            <td>{!! $farm->phosphorus !!}</td>
            <td>{!! $farm->potasium !!}</td>
            <td>{!! $farm->created_at !!}</td>
          
                <td>
                    {!! Form::open(['route' => ['farms.destroy', $farm->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('farms.show', [$farm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                       
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                      
                    </div>
                    {!! Form::close() !!}
                </td>
               
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
