<!-- Nitrogen Field -->
<div class="form-group col-sm-3">
    {!! Form::label('nitrogen', 'Nitrogen:') !!}
    {!! Form::text('nitrogen', null, ['class' => 'form-control']) !!}
</div>

<!-- Phosphorus Field -->
<div class="form-group col-sm-3">
    {!! Form::label('phosphorus', 'Phosphorus:') !!}
    {!! Form::text('phosphorus', null, ['class' => 'form-control']) !!}
</div>

<!-- Potasium Field -->
<div class="form-group col-sm-3">
    {!! Form::label('potasium', 'Potasium:') !!}
    {!! Form::text('potasium', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-3">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- System Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('system_id', 'System Id:') !!}
    {!! Form::number('system_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('farms.index') !!}" class="btn btn-default">Cancel</a>
</div>
