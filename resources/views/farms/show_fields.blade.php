
<!-- Nitrogen Field -->
<div class="form-group col-md-3">
    {!! Form::label('nitrogen', 'Nitrogen') !!}
    <p>{!! $farm->nitrogen !!}</p>
</div>

<!-- Phosphorus Field -->
<div class="form-group col-md-3">
    {!! Form::label('phosphorus', 'Phosphorus') !!}
    <p>{!! $farm->phosphorus !!}</p>
</div>

<!-- Potasium Field -->
<div class="form-group col-md-3">
    {!! Form::label('potasium', 'Potasium') !!}
    <p>{!! $farm->potasium !!}</p>
</div>

<!-- System Id Field -->
<div class="form-group col-md-3">
    {!! Form::label('system_id', 'System Model') !!}
    <p>{!! $farm->system['name'] !!} Mod: {!! $farm->system['model'] !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-md-12">
    {!! Form::label('created_at', 'Time') !!}
    <p>{!! $farm->created_at->format('d M Y,H:i:s')!!}</p>
</div>
@if(Auth::user()->id==1)

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User :') !!}
    <p>{!! $farm->user['email'] !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $farm->updated_at !!}</p>
</div>

{{-- <!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $farm->deleted_at !!}</p>
</div> --}}
@endif
