@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Farms</h1>
        <h1 class="pull-right">
      @if(Auth::user()->role_id==1)   
        <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('farms.create') !!}">Add New</a>
        @endif 
    </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <script type="text/javascript">

            var visitor = <?php echo $visitor; ?>;
      
            console.log(visitor);
      
            google.charts.load('current', {'packages':['corechart']});
      
            google.charts.setOnLoadCallback(drawChart);
      
            function drawChart() {
      
              var data = google.visualization.arrayToDataTable(visitor);
      
              var options = {
      
                title: 'Soil Fertility [NPK] Chart',
      
                curveType: 'function',
      
                legend: { position: 'bottom' }
      
              };
      
              var chart = new google.visualization.LineChart(document.getElementById('linechart'));
      
              chart.draw(data, options);
      
            }
      
          </script>
      
          <div id="linechart" style="width: 700px; height: 200px"></div>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
              
               
                    @include('farms.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

