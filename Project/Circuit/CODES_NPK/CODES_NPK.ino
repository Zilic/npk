#include <LiquidCrystal.h>
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);
int DEBUG = true;
int connectionId;
#define indicator 8

void setup() {
  Serial.begin(9600);
  lcd.begin(20, 4);
  lcd.print("Initializing...");
  lcd.setCursor(3, 1);
  lcd.print("NPK CHECKING");
  lcd.setCursor(6, 2);
  lcd.print("SYSTEM");
  pinMode(indicator, OUTPUT);
  //NODEMCU-12 INIT
//  sendData("AT+RST\r\n", 2000, DEBUG); // reset module
//  sendData("AT+CWMODE=2\r\n", 1000, DEBUG); // configure as access point
//  sendData("AT+CIFSR\r\n", 1000, DEBUG); // get ip address
//  sendData("AT+CIPMUX=1\r\n", 1000, DEBUG); // configure for multiple connections
//  sendData("AT+CIPSERVER=1,80\r\n", 1000, DEBUG); // turn on server on port 80
  //delay(500);
  lcd.clear();
  digitalWrite(indicator, HIGH);
}

void loop() {
  int sensorPin = analogRead(A0);
  float NITROGEN = map(sensorPin, 0, 1023, 1, 25);// NITROGEN Sensor Sensor
  float PHOSPHORUS = map(sensorPin, 0, 1023, 1, 50);// PHOSPHORUS Sensor Sensor
  float POTASSIUM = map(sensorPin, 0, 1023, 1, 200);// POTASSIUM Sensor Sensor
  
  //Calculations

  //Calculations
  //LCD printing
  lcd.setCursor(0, 0);
  lcd.print("----NPK SENSING----");
  lcd.setCursor(0, 1);
  lcd.print("N: ");
  lcd.print(NITROGEN);
  lcd.print(" ppm");

  lcd.setCursor(0, 2);
  lcd.print("P: ");
  lcd.print(PHOSPHORUS);
  lcd.print(" ppm");

  lcd.setCursor(0, 3);
  lcd.print("K: ");
  lcd.print(POTASSIUM);
  lcd.print(" ppm");
  //LCD printing
  //NODE SENDING..
  Serial.print("NITROGEN = ");
  Serial.print(NITROGEN);
  Serial.println(" ppm");
  Serial.print("PHOSPHORUS = ");
  Serial.print(PHOSPHORUS);
  Serial.println(" ppm");
  Serial.print("POTASSIUM = ");
  Serial.print(POTASSIUM);
  Serial.println(" ppm");
  Serial.println("----------------");
  
}

//////////////////////////////sends data from ESP to webpage///////////////////////////

void espsend(String d)
{
  String cipSend = " AT+CIPSEND=";

  cipSend += connectionId;
  cipSend += ",";
  cipSend += d.length();
  cipSend += "\r\n";
  sendData(cipSend, 1000, DEBUG);
  sendData(d, 1000, DEBUG);
}

//////////////gets the data from esp and displays in Serial monitor///////////////////////FEEDBACK
String sendData(String command, const int timeout, boolean debug)
{
  String response = "";
  Serial.print(command);
  long int time = millis();
  while ( (time + timeout) > millis())
  {
    while (Serial.available())
    {
      char c = Serial.read(); // read the next character.
      response += c;
    }
  }

  if (debug)
  {
    Serial.print(response); //displays the esp response messages in arduino Serial monitor
  }
  return response;
}
