<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class farm
 * @package App\Models
 * @version June 12, 2019, 4:17 pm UTC
 *
 * @property string nitrogen
 * @property string phosphorus
 * @property string potasium
 * @property integer user_id
 * @property integer system_id
 */
class farm extends Model
{
    use SoftDeletes;

    public $table = 'farms';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nitrogen',
        'phosphorus',
        'potasium'
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nitrogen' => 'string',
        'phosphorus' => 'string',
        'potasium' => 'string'
      
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nitrogen' => 'required',
        'phosphorus' => 'required',
        'potasium' => 'required'
       
    ];
    public function user(){

        return $this->hasOne('App\Models\user','id','user_id');
    }
    public function system(){

        return $this->hasOne('App\Models\system','id','system_id');
    }
    
    
}
