<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class system
 * @package App\Models
 * @version June 12, 2019, 4:16 pm UTC
 *
 * @property string name
 * @property string lname
 * @property string model
 * @property integer status_id
 */
class system extends Model
{
    use SoftDeletes;

    public $table = 'systems';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
     
        'model',
        'status_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
       
        'model' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
       
        'model' => 'required',
        'status_id' => 'required'
    ];

    public function status(){

        return $this->hasOne('App\Models\status','id','status_id');
    }
}
