<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class user
 * @package App\Models
 * @version June 12, 2019, 4:16 pm UTC
 *
 * @property string fname
 * @property string lname
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string phone_no
 * @property string address
 * @property integer role_id
 * @property string remember_token
 */
class user extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fname',
        'lname',
        'email',
        'email_verified_at',
        'password',
        'phone_no',
        'address',
        'role_id',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fname' => 'string',
        'lname' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'phone_no' => 'string',
        'address' => 'string',
        'role_id' => 'integer',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fname' => 'required',
        'lname' => 'required',
        'email' => 'required',
        'password' => 'required',
        'phone_no' => 'required',
        'address' => 'required',
        'role_id' => 'required'
    ];
    public function role(){

        return $this->hasOne('App\Models\role','id','role_id');
    }
    
}
