<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatesystemRequest;
use App\Http\Requests\UpdatesystemRequest;
use App\Repositories\systemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class systemController extends AppBaseController
{
    /** @var  systemRepository */
    private $systemRepository;

    public function __construct(systemRepository $systemRepo)
    {
        $this->systemRepository = $systemRepo;
    }

    /**
     * Display a listing of the system.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $systems = $this->systemRepository->all();

        return view('systems.index')
            ->with('systems', $systems);
    }

    /**
     * Show the form for creating a new system.
     *
     * @return Response
     */
    public function create()
    {
        return view('systems.create');
    }

    /**
     * Store a newly created system in storage.
     *
     * @param CreatesystemRequest $request
     *
     * @return Response
     */
    public function store(CreatesystemRequest $request)
    {
        $input = $request->all();

        $system = $this->systemRepository->create($input);

        Flash::success('System saved successfully.');

        return redirect(route('systems.index'));
    }

    /**
     * Display the specified system.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $system = $this->systemRepository->find($id);

        if (empty($system)) {
            Flash::error('System not found');

            return redirect(route('systems.index'));
        }

        return view('systems.show')->with('system', $system);
    }

    /**
     * Show the form for editing the specified system.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $system = $this->systemRepository->find($id);

        if (empty($system)) {
            Flash::error('System not found');

            return redirect(route('systems.index'));
        }

        return view('systems.edit')->with('system', $system);
    }

    /**
     * Update the specified system in storage.
     *
     * @param int $id
     * @param UpdatesystemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesystemRequest $request)
    {
        $system = $this->systemRepository->find($id);

        if (empty($system)) {
            Flash::error('System not found');

            return redirect(route('systems.index'));
        }

        $system = $this->systemRepository->update($request->all(), $id);

        Flash::success('System updated successfully.');

        return redirect(route('systems.index'));
    }

    /**
     * Remove the specified system from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $system = $this->systemRepository->find($id);

        if (empty($system)) {
            Flash::error('System not found');

            return redirect(route('systems.index'));
        }

        $this->systemRepository->delete($id);

        Flash::success('System deleted successfully.');

        return redirect(route('systems.index'));
    }
}
