<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatefarmRequest;
use App\Http\Requests\UpdatefarmRequest;
use App\Repositories\farmRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use validator;
use Flash;
use Response;
use Charts;
use App\Models\farm;
use DB;

class farmController extends AppBaseController
{
    /** @var  farmRepository */
    private $farmRepository;

    public function __construct(farmRepository $farmRepo)
    {
        $this->farmRepository = $farmRepo;
    }
   
    /**
     * Display a listing of the farm.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $farms = $this->farmRepository->all();


        $visitor = DB::table('farms')

                    ->select(
                        // DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),

                        // DB::raw("year(created_at) as year"),
                        DB::raw("month(created_at) as month"),

                        DB::raw("SUM(nitrogen) as total_nitrogen"),

                        DB::raw("SUM(phosphorus) as total_phosphorus"),
                        DB::raw("SUM(potasium) as total_potasium")) 

                    ->orderBy("created_at")

                    ->groupBy(DB::raw("month(created_at)"))

                    ->get();



        $result[] = ['month','nitrogen','phosphorus','potasium'];

        foreach ($visitor as $key => $value) {

            $result[++$key] = [$value->month, (int)$value->total_nitrogen, (int)$value->total_phosphorus, (int)$value->total_potasium];

        }


        return view('farms.index')
            ->with('farms', $farms)
            ->with('visitor',json_encode($result));
    }
    public function lineChart()

    {

        $visitor = DB::table('farms')

                    ->select(
                        // DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),

                        // DB::raw("year(created_at) as year"),
                        DB::raw("month(created_at) as month"),

                        DB::raw("SUM(nitrogen) as total_nitrogen"),

                        DB::raw("SUM(phosphorus) as total_phosphorus"),
                        DB::raw("SUM(potasium) as total_potasium")) 

                    ->orderBy("created_at")

                    ->groupBy(DB::raw("month(created_at)"))

                    ->get();



        $result[] = ['month','nitrogen','phosphorus','potasium'];

        foreach ($visitor as $key => $value) {

            $result[++$key] = [$value->month, (int)$value->total_nitrogen, (int)$value->total_phosphorus, (int)$value->total_potasium];

        }



        return view('charts')

                ->with('visitor',json_encode($result));

    }
    /**
     * Show the form for creating a new farm.
     *
     * @return Response
     */
    public function createApi(Request $request){
        $farms=new farm;

        $farms->nitrogen = $request->input('nitrogen');
        $farms->phosphorus = $request->input('phosphorus');
        $farms->potasium = $request->input('potasium');

        $farms->save();
     
    }

    
    public function create()
    {
        return view('farms.create');
    }

    /**
     * Store a newly created farm in storage.
     *
     * @param CreatefarmRequest $request
     *
     * @return Response
     */
    public function store(CreatefarmRequest $request)
    {
        $input = $request->all();

        $farm = $this->farmRepository->create($input);

        Flash::success('Farm saved successfully.');

        return redirect(route('farms.index'));
    }

    /**
     * Display the specified farm.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $farm = $this->farmRepository->find($id);

        if (empty($farm)) {
            Flash::error('Farm not found');

            return redirect(route('farms.index'));
        }

        return view('farms.show')->with('farm', $farm);
    }

    /**
     * Show the form for editing the specified farm.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $farm = $this->farmRepository->find($id);

        if (empty($farm)) {
            Flash::error('Farm not found');

            return redirect(route('farms.index'));
        }

        return view('farms.edit')->with('farm', $farm);
    }

    /**
     * Update the specified farm in storage.
     *
     * @param int $id
     * @param UpdatefarmRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatefarmRequest $request)
    {
        $farm = $this->farmRepository->find($id);

        if (empty($farm)) {
            Flash::error('Farm not found');

            return redirect(route('farms.index'));
        }

        $farm = $this->farmRepository->update($request->all(), $id);

        Flash::success('Farm updated successfully.');

        return redirect(route('farms.index'));
    }

    /**
     * Remove the specified farm from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $farm = $this->farmRepository->find($id);

        if (empty($farm)) {
            Flash::error('Farm not found');

            return redirect(route('farms.index'));
        }

        $this->farmRepository->delete($id);

        Flash::success('Farm deleted successfully.');

        return redirect(route('farms.index'));
    }
}
