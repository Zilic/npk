<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\farm;
use Charts;
use DB;
class chartController extends Controller
{
    public function index(){

        $npk = farm::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))->get();
        $chart = Charts::database($npk,'line','highcharts')
                        ->title('NPK')
                        ->elementLabel('Values')
                        ->dimensions(1000,500)
                        ->responsive(false)
                        ->groupByMonth(date('Y'),true);
     return view('charts',compact('chart'));


    }
// public function highchart()

// {

//     $viewer = View::select(DB::raw("SUM(numberofview) as count"))

//         ->orderBy("created_at")

//         ->groupBy(DB::raw("year(created_at)"))

//         ->get()->toArray();

//     $viewer = array_column($viewer, 'count');

    

//     $click = Click::select(DB::raw("SUM(numberofclick) as count"))

//         ->orderBy("created_at")

//         ->groupBy(DB::raw("year(created_at)"))

//         ->get()->toArray();

//     $click = array_column($click, 'count');

//     return view('highchart')

//             ->with('viewer',json_encode($viewer,JSON_NUMERIC_CHECK))

//             ->with('click',json_encode($click,JSON_NUMERIC_CHECK));

// }
public function googleLineChart()

    {

        $visitor = DB::table('farms')

                    ->select(
                        // DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),

                        // DB::raw("year(created_at) as year"),
                        DB::raw("month(created_at) as month"),

                        DB::raw("SUM(nitrogen) as total_nitrogen"),

                        DB::raw("SUM(phosphorus) as total_phosphorus"),
                        DB::raw("SUM(potasium) as total_potasium")) 

                    ->orderBy("created_at")

                    ->groupBy(DB::raw("month(created_at)"))

                    ->get();



        $result[] = ['month','nitrogen','phosphorus','potasium'];

        foreach ($visitor as $key => $value) {

            $result[++$key] = [$value->month, (int)$value->total_nitrogen, (int)$value->total_phosphorus, (int)$value->total_potasium];

        }



        return view('charts')

                ->with('visitor',json_encode($result));

    }
}
