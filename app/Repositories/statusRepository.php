<?php

namespace App\Repositories;

use App\Models\status;
use App\Repositories\BaseRepository;

/**
 * Class statusRepository
 * @package App\Repositories
 * @version June 12, 2019, 4:18 pm UTC
*/

class statusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return status::class;
    }
}
