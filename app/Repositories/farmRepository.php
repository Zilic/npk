<?php

namespace App\Repositories;

use App\Models\farm;
use App\Repositories\BaseRepository;

/**
 * Class farmRepository
 * @package App\Repositories
 * @version June 12, 2019, 4:17 pm UTC
*/

class farmRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nitrogen',
        'phosphorus',
        'potasium',
        'user_id',
        'system_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return farm::class;
    }
}
