<?php

namespace App\Repositories;

use App\Models\system;
use App\Repositories\BaseRepository;

/**
 * Class systemRepository
 * @package App\Repositories
 * @version June 12, 2019, 4:16 pm UTC
*/

class systemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'lname',
        'model',
        'status_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return system::class;
    }
}
