<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::group(['middleware'=>'App\Http\Middleware\admin'],function(){
    Route::resource('users', 'userController');

    Route::resource('systems', 'systemController');
    Route::resource('roles', 'roleController');

    Route::resource('statuses', 'statusController');
});



Route::resource('farms', 'farmController');
Route::get('charts', 'chartController@googleLineChart')->name('chart.index');
// Route::get('charts', 'chartController@index')->name('chart.index');

